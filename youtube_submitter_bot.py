import time
import praw
import prawcore
import OAuth2Util
from datetime import datetime, timedelta
from apiclient.discovery import build

# User config
# --------------------------------------------------------------------
DEVELOPER_KEY = ''
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

# Populate with information for the account you want the bot to run on
CLIENT_ID = ''
CLIENT_SECRET = ''
CLIENT_USERNAME = ''
CLIENT_PASSWORD = ""

# The subreddit you wish to post to
SUBREDDIT_NAME = ''

# The channel ID of the channel you wish to repost videos from
CHANNEL_IDS = ['']

# Date of the first video you wish to psot from
FIRST_VIDEO_DATE = datetime(2016, 4, 1)

# How long you want it to wait before searching for
# videos again in seconds (default is 86400, a day)
WAIT_TIME = 86400 # One day

# Set how many results you want it to return
# reccommended is 5, set higher if your channels
# produce a lot of videos
MAX_RESULTS = 5

# Set true if you want the channel name in the title
ADD_CHANNEL_NAME_TO_TITLE = False

# Make the post distinguished and sticky
DISTINGUISH_AND_STICKY = True

# Insert the CSS flair for the post
FLAIR_TEXT_CSS = ''

# Flair ID. This can be found by viewing the flairs as a moderator on the site or with the code in the post_to_reddit method
FLAIR_ID_CSS = ''

# Else set static flair text
LINK_FLAIR_TEXT = ''

# --------------------------------------------------------------------

def post_to_reddit(r, video):
	subm = ""
	if ADD_CHANNEL_NAME_TO_TITLE:
		title = '[{}] {}'.format(video[2], video[0])
	else:
		title = video[0]

	link = 'https://www.youtube.com/watch?v={}'.format(video[1])

	try:
		print('Submitting {}...'.format(title))
		print(link)
		# Link post code
		subm = r.subreddit(SUBREDDIT_NAME).submit(title, url=link, flair_id=FLAIR_ID_CSS, flair_text=FLAIR_TEXT_CSS)
		# If you want the post to be a text post use this code
		# subm = r.subreddit("TheCowChop").submit(title, flair_id='35d72acc-1095-11e6-a78c-0e462b3a24fb', flair_text=':mod: Mod Post', selftext = link)

		# Destinguish and make the post a sticky
		if DISTINGUISH_AND_STICKY:
			subm.mod.distinguish(how="yes", sticky=True)
			subm.mod.sticky()

		# Uncomment this code to see the flair choices including their flair_ids
		# choices = subm.flair.choices()
		# print(choices)
		return subm
	except Exception as e:
		print('ERROR: ' + str(e))
		return None

def youtube_search(channel_ID, pubAfter, pubBefore):
	youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
					developerKey=DEVELOPER_KEY)
	# Search youtube for videos amtching this date range. NOTE: Youtube publishedAt data is not reflected on youtube's site. These are not exact as 
	# Youtube does not allow you to access the exact time the video was posted. An hour buffer is given which seems to correct this but is not perfect
	search_response = youtube.search().list(
						channelId=channel_ID,
						order='date',
						part='id, snippet',
						maxResults=MAX_RESULTS,
						type='video',
						publishedAfter=str(pubAfter.year) + '-' + '{:02d}'.format(pubAfter.month) + '-' + '{:02d}'.format(pubAfter.day) + 'T01:00:00Z',
						publishedBefore=str(pubBefore.year) + '-' + '{:02d}'.format(pubBefore.month) + '-' + '{:02d}'.format(pubBefore.day) + 'T01:00:00Z'
						).execute()

	videos = []
	for search_result in search_response.get('items', []):
		videos.append([search_result['snippet']['title'],
					   search_result['id']['videoId'],
					   search_result['snippet']['channelTitle'],
					   search_result['snippet']['publishedAt']])

	return videos

def main():

	submissions = []

	f = open('count.txt', 'r')
	# Populate this information with the reddit account you want to run the bot on
	r = praw.Reddit(user_agent='Youtube_Reposter_Bot /u/FearTheZ', username = CLIENT_USERNAME, client_id=CLIENT_ID, client_secret=CLIENT_SECRET, password=CLIENT_PASSWORD)
	r.validate_on_submit = True
	
	# Used to kleep track of how many videos posted, increments
	# what date to pull video from. This can be kept as a variable
	# since the code is alwasy running but if the code is stopped the
	# text file will know where the bot left off
	DAYS_POSTED = int(f.read())

	while True:
		# Unsticky the old post if there is one
		if DISTINGUISH_AND_STICKY:
			if len(submissions) > 0:
				for subm in submissions:
					subm.mod.distinguish(how="no", sticky=False)
					subm.mod.sticky(state = False)
				submissions = []
		# Calculate which index to be searching for
		pubBefore = FIRST_VIDEO_DATE + timedelta(DAYS_POSTED)
		pubAfter = pubBefore + timedelta(1)
		# Test prints
		# print(pubBefore)
		# print(pubAfter)
		# print(str(pubBefore.year) + '-' + '{:02d}'.format(pubBefore.month) + '-' + '{:02d}'.format(pubBefore.day) + 'T23:59:59Z')

		# Try searching for the videos. NOTE: Each inquiry uses a bunch of requests for your Google dev account and 
		# the free accounts have a per day limit so be careful in running this quickly you may run out
		try:
			print('Getting video(s)...')
			videos = []
			for channel in CHANNEL_IDS:
				c_videos = youtube_search(channel, pubBefore, pubAfter)
				[videos.append(v) for v in c_videos]
			
			print(videos)			
			
			# Found no videos for today, go to the next day
			if len(videos) < 1:
				DAYS_POSTED = DAYS_POSTED + 1 #Increment the day since we posted for today
				f = open('count.txt', 'w')
				f.write(str(DAYS_POSTED))
				continue

			for video in videos:
				if DISTINGUISH_AND_STICKY:
					submissions.append(post_to_reddit(r, video))
				else:
					post_to_reddit(r, video)

			DAYS_POSTED = DAYS_POSTED + 1 #Increment the day since we posted for today
			f = open('count.txt', 'w')
			f.write(str(DAYS_POSTED))
			

			if WAIT_TIME:
				print('Sleeping...')
				time.sleep(WAIT_TIME)
			else:
				print('No new videos vound, incrementing and trying again...')
				
		except Exception as e:
			print('ERROR: ' + str(e))
			time.sleep(500)


if __name__ == '__main__':
	# try to import my testing config
	try:
		import bot
		SUBREDDIT_NAME = bot.SUBREDDIT_NAME
		DEVELOPER_KEY = bot.DEVELOPER_KEY
		CHANNEL_IDS = bot.CHANNEL_IDS
	except ImportError:
		pass

	if not DEVELOPER_KEY:
		print('ERROR: You need to add your google developer key before ' +
			  'this can run')
		print('Exiting...')
		exit(1)
	if not SUBREDDIT_NAME:
		print('ERROR: You need to add a subreddit name before this can run')
		print('Exiting...')
		exit(1)
	if not CHANNEL_IDS:
		print('ERROR: You need to add channel ids before this can run')
		print('Exiting...')
		exit(1)

	main()
